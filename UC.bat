CALL D:\Users\Administrator\ClearDriver.bat
cd /D D:\RobotAutomation\UC
del /S *.png
del /S *.pyc
del D:\RobotAutomation\UC\original.xml
del D:\RobotAutomation\UC\rerun.xml
del D:\RobotAutomation\UC\report.html
del D:\RobotAutomation\UC\log.html
REM CALL pybot --output D:\RobotAutomation\UC\original.xml --Variable env:prodna -t BROWSER:D:\RobotAutomation\UC\chrome -t 01__Login -t 041__Administration_Member_ExportValidation D:\RobotAutomation\UC\UserCentric.robot 
CALL pybot --output D:\RobotAutomation\UC\original.xml --Variable env:prodna -t BROWSER:D:\RobotAutomation\UC\chrome -t 01__Login -t 02__LoginValidation -t 04__AddAndDeleteAutomateCompnayWithDashboardValidation -t 05__AddAndDeleteUserCentricCompanyWithDashboardValidation -t 06__AddCompanyExistValidation -t 07__CompaniesDashboardSearchAndExport_Validation -t 012__ConflictValidationForFirstNameAndLastName -t 013__ConflictValidationForEmail -t 014__ConnectWiseContactIgnoreValidation -t 015__AgreementTab_DefaultBillingOptionCheckUnCheckOptionValidation -t 016__BPAssignedAndUnassignedFromAllContactTabValidate -t 017__IgnoreContactTab_UserReset_Validate_WithdSyncVerify -t 018__IgnoreContactTab_MassReset_Validate_WithdSyncVerify -t 025__Administration_BillingProfile_SimplePrice_AddEditDeleteValidation -t 026__Administration_BillingProfile_TiredPrice_AddEditDeleteValidation -t 027__Administration_BillingProfile_VolumePrice_AddEditDeleteValidation -t 028__Administration_BillingProfile_All3Price_AddEditDeleteValidation -t 029__Adminsitration_BillingProfile_UsedBPDelete_And_ExistingBP_Validation -t 030__Administration_MemberTab_AssignAdministratorAccess_Vaidation -t 031__Administration_MemberTab_AssignFinanceAccess_Vaidation -t 032__Administration_MemberTab_AssignTechnicianAccess_Vaidation -t 033__Administration_MemberTab_AssignNoAccess_Vaidation -t 034__Administration_AutomateTab_AddEditDelete_Validation -t 035__Administration_SettingsTab_Uncheck_ValidationInCompanyLevel -t 036__Adminsitration_SettingsTab_CheckAllTheSettingsOption_ValidationInCompanyLevel -t 038__Administration_BillingProfile_ProfileNameChangeReflectToCompanyLevel_Validation -t 039__Administration_BillingProfile_ProductChangeReflectToCompanyLevel_Validation -t 040__Administration_BillingProfile_ExportValidation -t 041__Administration_Member_ExportValidation -t 042__Administration_Automate_ExportValidation D:\RobotAutomation\UC\UserCentric.robot
CALL pybot  --rerunfailed D:\RobotAutomation\UC\original.xml --output D:\RobotAutomation\UC\rerun.xml --Variable env:prodna -t 01__Login D:\RobotAutomation\UC\UserCentric.robot
CALL rebot --merge D:\RobotAutomation\UC\original.xml D:\RobotAutomation\UC\rerun.xml
CALL python D:\Users\Administrator\UCMailSend.py
CALL taskkill /F /IM chrome.exe /T > nul
CALL taskkill /F /IM firefox.exe /T > nul
CALL taskkill /F /IM cmd.exe /T > nul
REM CALL taskkill /F /IM chromedriver.exe /T > nul
REM CALL taskkill /F /IM geckodriver.exe /T > nul
